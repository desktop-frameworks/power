# DFL::Power

DFL Power Manager simplifies power management. It performs various tasks such as changing the brightnesson idle,
suspend on lid close and so on, based on the user provided power profiles. It's also capable of switching off screens
for power saving. An implementation screen color temperature, and gamma tables based brightness for LED/OLD/AMOLED
dispays is available.


### Dependencies:
* <tt>Qt5 Core and DBus (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>WayQt (https://gitlab.com/desktop-frameworks/wayqt.git</tt>
* <tt>WlrGammaEffects (https://gitlab.com/desktop-frameworks/gamma-effects.git</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/power.git dfl-power`
- Enter the `dfl-power` folder
  * `cd dfl-power`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any feature that you ask for.
