/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The UPowerBackend watches the org.freedesktop.UPower and
 *  relays the relevent information to DFL::PowerManager class.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>

namespace DFL {
    class Login1;

    namespace Power {
        namespace Module {
            class UPower;
            class Wlroots;
        }
    }
}

typedef struct power_manager_implt_t {
    /** Remember the current state and configuration */
    DFL::Power::PowerState                   mCurState;
    DFL::Power::LidState                     mCurLidState;
    DFL::Power::PowerStateConfig             mCfg;

    /** UPower module instance which will tell use about various power changes */
    DFL::Power::Module::UPower               *UPower;

    /** Login1 instance to perform various login/session actions */
    DFL::Login1                              *Login1;

    /** UPower module instance to setup sunset/soft-brightness modes */
    DFL::Power::Module::Wlroots              *Wlroots;

    /** Remember the various power configurations */
    QHash<int, DFL::Power::PowerStateConfig> mPowerConfigs;

    /** Low and Critical Battery levels */
    double                                   mLowCharge      = 15.0;
    double                                   mCriticalCharge = 2.0;
} PowerManagerImpl;
