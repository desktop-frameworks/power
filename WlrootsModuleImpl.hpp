/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The WlrootsImpl struct is a container for variables of Wlroots
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>

/** WayQt Headers */
#include <wayqt/Idle.hpp>
#include <wayqt/Output.hpp>
#include <wayqt/WlrIdle.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/GammaControl.hpp>
#include <wayqt/OutputPowerManager.hpp>

/** DFL Headers */
#include <DFGammaEffects.hpp>

typedef struct wlroots_impl_t {
    /** Entry point to Wayland/Wlroots */
    WQt::Registry                           *registry = nullptr;

    /** List of attached outputs */
    QHash<QString, WQt::Output *>           outputs;

    /**
     * Idle Manager and IdleWatch objects
     * The IdleWatch will wait for the system to become idle in a given time
     */
    WQt::IdleManager                        *wlrIdleMgr   = nullptr;
    WQt::IdleWatcher                        *wlrIdleWatch = nullptr;

    /**
     * Idle Manager and IdleWatch objects
     * The IdleWatch will wait for the system to become idle in a given time
     */
    WQt::IdleNotifier                       *idleMgr   = nullptr;
    WQt::IdleNotification                   *idleWatch = nullptr;

    /**
     * OutputPower Manager and Power objects
     * The OutputPower objects will perform the DPMS actions (1 per output)
     */
    WQt::OutputPowerManager                 *opMgr = nullptr;
    QHash<QString, WQt::OutputPower *>      dpms;

    /**
     * Gamma Conrtols Manager to obtain the Gamma Conrtols for each outupt.
     * The gamma effects for each of the outputs corresponding to each
     * Gamma Control.
     */
    WQt::GammaControlManager                *gammaMgr = nullptr;
    QHash<QString, WQt::GammaControl *>     gammaCtrls;
    QHash<QString, DFL::GammaEffects *>     gammaEffects;

    /**
     * These clients rely on GammaControl to set their brightness.
     */
    QStringList                             brightnessClients;

    /** GammaEffects config objects, one per output */
    QHash<QString, DFL::GammaEffectsConfig> gammaConfigs;
    DFL::GammaEffectsConfig                 gammaRefConfig;

    /** Sunset mode */
    bool                                    sunsetMode = false;

    /** Pending timeout setup */
    int                                     pendingTimeout = -1;
} WlrootsImpl;
