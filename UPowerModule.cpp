/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The UPowerModule watches the org.freedesktop.UPower and
 *  relays the relevent information to DFL::PowerManager class.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#include "UPowerModule.hpp"
#include "UPowerModuleImpl.hpp"

DFL::Power::Module::UPower::UPower( QObject *parent ) : QObject( parent ) {
    impl = new UPowerImpl;

    impl->upower = new QDBusInterface(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.UPower",
        QDBusConnection::systemBus()
    );

    /**
     * We need an object path based on which we can take power decisions.
     * The first object of type 'Battery' and 'PowerSupply=true', will be used.
     */
    QDBusReply<QList<QDBusObjectPath> > devReply = impl->upower->call( "EnumerateDevices" );

    if ( devReply.isValid() ) {
        for ( QDBusObjectPath objPath: devReply.value() ) {
            QDBusInterface *iface = new QDBusInterface(
                "org.freedesktop.UPower",
                objPath.path(),
                "org.freedesktop.UPower.Device",
                QDBusConnection::systemBus()
            );

            if ( (iface->property( "Type" ).toInt() == 2) and (iface->property( "PowerSupply" ).toBool() == true) ) {
                impl->display = iface;
                break;
            }

            else {
                delete iface;
            }
        }
    }

    /** if we did not find any suitable device, let's use UPower's DisplayDevice */
    if ( impl->display == nullptr ) {
        impl->display = new QDBusInterface(
            "org.freedesktop.UPower",
            "/org/freedesktop/UPower/devices/DisplayDevice",
            "org.freedesktop.UPower.Device",
            QDBusConnection::systemBus()
        );
    }

    /* Battery/PowerSupply changes */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        impl->display->path(),
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT( handlePowerChanges( QString, QVariantMap, QStringList ) )
    );

    /* For LidClosed/OnBattery changes */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT( handlePowerChanges( QString, QVariantMap, QStringList ) )
    );

    impl->mLidOpen        = not impl->upower->property( "LidIsClosed" ).toBool();
    impl->mOnBattery      = impl->upower->property( "OnBattery" ).toBool();
    impl->mLastPercentage = impl->display->property( "Percentage" ).toDouble();
}


qlonglong DFL::Power::Module::UPower::getTimeToFull() {
    impl->mETF = impl->display->property( "TimeToFull" ).toLongLong();
    return impl->mETF;
}


qlonglong DFL::Power::Module::UPower::getTimeToEmpty() {
    impl->mETR = impl->display->property( "TimeToEmpty" ).toLongLong();
    return impl->mETR;
}


bool DFL::Power::Module::UPower::onBattery() {
    impl->mOnBattery = impl->upower->property( "OnBattery" ).toBool();
    return impl->mOnBattery;
}


double DFL::Power::Module::UPower::batteryCharge() {
    /* Update mLastPercentage */
    impl->mLastPercentage = impl->display->property( "Percentage" ).toDouble();
    return impl->mLastPercentage;
}


void DFL::Power::Module::UPower::handlePowerChanges( QString, QVariantMap, QStringList ) {
    /* Check for Lid State Changes */
    bool lidOpen = not impl->upower->property( "LidIsClosed" ).toBool();

    if ( lidOpen != impl->mLidOpen ) {
        impl->mLidOpen = lidOpen;

        emit lidStateChanged( impl->mLidOpen );
    }

    bool battery = impl->upower->property( "OnBattery" ).toBool();

    /* Battery percentage */
    double curBatPC = impl->display->property( "Percentage" ).toDouble();

    if ( impl->mLastPercentage != curBatPC ) {
        impl->mLastPercentage = curBatPC;
        emit batteryChargeChanged( impl->mLastPercentage );
    }

    if ( battery != impl->mOnBattery ) {
        impl->mOnBattery = battery;

        if ( impl->mOnBattery ) {
            emit switchedToBattery();
            impl->mETR = impl->display->property( "TimeToEmpty" ).toLongLong();
        }

        else {
            emit switchedToACPower();
            impl->mETF = impl->display->property( "TimeToFull" ).toLongLong();
        }
    }

    if ( battery ) {
        impl->mETR = impl->display->property( "TimeToEmpty" ).toLongLong();

        emit timeToEmpty( impl->mETR );

        bool isCritical = (impl->mETR <= 60) or (impl->mLastPercentage <= 1.0);
        bool isLow      = (impl->mETR <= 300) or (impl->mLastPercentage <= 5.0);

        /**
         * If only around 1m/1% remains, intimate the user
         */
        if ( isCritical and (not impl->mSignalled60) ) {
            emit batteryEmpty();
            impl->mSignalled60 = true;
        }

        /*
         * If only around 5m/5% remains, intimate the user
         */
        else if ( isLow and (not impl->mSignalled300) ) {
            emit batteryNearlyEmpty();
            impl->mSignalled300 = true;
        }
    }

    else {
        impl->mETF = impl->display->property( "TimeToFull" ).toLongLong();

        emit timeToFull( impl->mETF );

        /*
         * Reset the signalled flags when charging
         * This way, user is informed again when the power is lost
         */
        impl->mSignalled60  = false;
        impl->mSignalled300 = false;
    }
}
