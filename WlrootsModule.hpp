/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  This class manages all objects/functions relating to Wayland/Wlroots.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>

#include "WlrootsModuleImpl.hpp"

namespace DFL {
    namespace Power {
        namespace Module {
            class Wlroots;
        }
    }
}

class DFL::Power::Module::Wlroots : public QObject {
    Q_OBJECT;

    public:
        Wlroots( QObject *parent = nullptr );
        ~Wlroots();

        /**
         * Idle Watch
         * Setup and run WQt::IdleWatch.
         * Emit a signal when the IdleWatch times out.
         * Also emit a signal when Activity resumes after a idle watch.
         */
        void setupIdleWatch( int timeout );

        /**
         * If you monitor supports DPMS, we can perform a DPMS Off/On
         */
        void powerOffDisplays();
        void powerOnDisplays();

        /**
         * Set/Get brightness using Gamma Effects.
         */
        qreal getBrightness();
        void setBrightness( qreal );

        /**
         * Set/Get displays that use Gamma Effects for brightness
         */
        QStringList brightnessClients();
        void addBrightnessClient( QString );
        void removeBrightnessClient( QString );

        /**
         * Enable/Disable Sunset mode
         */
        void setSunsetMode( bool );

        /**
         * Update GammaConfig
         * It's preferable that all the parameters to be changed are set at once
         */
        void updateGammaConfig( DFL::GammaEffectsConfig cfg );

    private:
        WlrootsImpl *impl;

        /**
         * When an output is added, we will setup the corresponding
         * GammaControls and GammaEffects, as well as OutputPower objects.
         */
        void handleOutputAdded( WQt::Output *op );

        /**
         * When an output is removed, we will remove the corresponding
         * GammaControls, GammaEffects, and the OutputPower objects.
         */
        void handleOutputRemoved( WQt::Output *op );

        /**
         * Sometimes GammaControl dies. We should reset it.
         */
        void restartGammaEffects( WQt::Output *op );

    Q_SIGNALS:
        /** Triggered when no user activity was registered in the requested idle time interval */
        void idleTimedOut();

        /** Triggered on the first user activity after an idle event */
        void activityResumed();
};
