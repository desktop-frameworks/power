/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The PowerProfile class loads the user's choice and implements it.
 *  This is the main work-horse of DFL::Power::Manager.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#include "WlrootsModule.hpp"
#include "WlrootsModuleImpl.hpp"

DFL::Power::Module::Wlroots::Wlroots( QObject *parent ) : QObject( parent ) {
    impl = new WlrootsImpl;

    /** Create the Wayland Registry Object */
    impl->registry = new WQt::Registry( WQt::Wayland::display() );

    connect(
        impl->registry, &WQt::Registry::interfaceRegistered, [ = ] ( WQt::Registry::Interface iface ) {
            switch ( iface ) {
                case WQt::Registry::IdleInterface: {
                    impl->idleMgr = impl->registry->idleNotifier();

                    if ( impl->pendingTimeout > 0 ) {
                        setupIdleWatch( impl->pendingTimeout );
                        impl->pendingTimeout = -1;
                    }

                    break;
                }

                case WQt::Registry::WlrIdleInterface: {
                    impl->wlrIdleMgr = impl->registry->idleManager();

                    if ( impl->pendingTimeout > 0 ) {
                        setupIdleWatch( impl->pendingTimeout );
                        impl->pendingTimeout = -1;
                    }

                    break;
                }

                default: {
                    break;
                }
            }
        }
    );

    impl->registry->setup();

    if ( impl->registry->waitForInterface( WQt::Registry::OutputPowerManagerInterface ) ) {
        impl->opMgr = impl->registry->outputPowerManager();
    }

    if ( impl->registry->waitForInterface( WQt::Registry::GammaControlManagerInterface ) ) {
        impl->gammaMgr = impl->registry->gammaControlManager();
    }

    /**
     * We're adding the outputs for the first time.
     */
    for ( WQt::Output *wlOp: impl->registry->waylandOutputs() ) {
        handleOutputAdded( wlOp );
    }

    /**
     * Now we can connect the new outputs added signal to slots
     * Also invoke the slots that remove the disconnected outputs
     */
    connect( impl->registry, &WQt::Registry::outputAdded,   this, &DFL::Power::Module::Wlroots::handleOutputAdded );
    connect( impl->registry, &WQt::Registry::outputRemoved, this, &DFL::Power::Module::Wlroots::handleOutputRemoved );
}


DFL::Power::Module::Wlroots::~Wlroots() {
    /** We don't want any signals from the registry */
    impl->registry->disconnect();

    /** Delete all the existing outputs, and related objects */
    for ( QString opStr: impl->outputs.keys() ) {
        WQt::Output *op = impl->outputs.take( opStr );
        handleOutputRemoved( op );
    }

    /** Delete the Idle objects */
    if ( impl->idleMgr != nullptr ) {
        delete impl->idleMgr;
    }

    if ( impl->idleWatch != nullptr ) {
        impl->idleWatch->destroy();

        delete impl->idleWatch;
    }

    /** Delete the Idle objects */
    if ( impl->wlrIdleMgr != nullptr ) {
        delete impl->wlrIdleMgr;
    }

    if ( impl->wlrIdleWatch != nullptr ) {
        impl->wlrIdleWatch->suspendWatch();
        impl->wlrIdleWatch->disconnect();

        delete impl->idleWatch;
    }

    /** Delete the output power manager */
    delete impl->opMgr;

    /** Delete the gamma control manager */
    delete impl->gammaMgr;

    /** Delete the registry */
    delete impl->registry;

    /** Finally delete the impl object */
    delete impl;
}


void DFL::Power::Module::Wlroots::setupIdleWatch( int timeout ) {
    /** Proceed only if one of the managers is available */
    if ( (impl->idleMgr == nullptr) && (impl->wlrIdleMgr == nullptr) ) {
        impl->pendingTimeout = -1;
        return;
    }

    if ( impl->idleWatch != nullptr ) {
        impl->idleWatch->destroy();
        delete impl->idleWatch;
        impl->idleWatch = nullptr;
    }

    if ( impl->wlrIdleWatch != nullptr ) {
        impl->wlrIdleWatch->suspendWatch();
        impl->wlrIdleWatch->disconnect();
        delete impl->wlrIdleWatch;
        impl->wlrIdleWatch = nullptr;
    }

    /** New version is available */
    if ( impl->idleMgr ) {
        impl->idleWatch = impl->idleMgr->createNotification( (uint32_t)timeout, impl->registry->waylandSeat() );
    }

    /** Use the old version */
    else {
        impl->wlrIdleWatch = impl->wlrIdleMgr->getIdleWatcher( impl->registry->waylandSeat(), timeout );
    }

    if ( (impl->idleWatch == nullptr) && (impl->wlrIdleWatch == nullptr) ) {
        return;
    }

    if ( impl->idleWatch != nullptr ) {
        connect( impl->idleWatch, &WQt::IdleNotification::idled, this, &DFL::Power::Module::Wlroots::idleTimedOut );

        /** once resumed is emitted, this object dies. We need to recreate the watch. */
        connect(
            impl->idleWatch, &WQt::IdleNotification::resumed, [ = ] () {
                emit activityResumed();

                setupIdleWatch( timeout );
            }
        );

        impl->idleWatch->setup();
    }

    else {
        connect( impl->wlrIdleWatch, &WQt::IdleWatcher::timedOut, this, &DFL::Power::Module::Wlroots::idleTimedOut );

        /** once resumed is emitted, this object dies. We need to recreate the watch. */
        connect(
            impl->wlrIdleWatch, &WQt::IdleWatcher::activityResumed, [ = ] () {
                emit activityResumed();

                setupIdleWatch( timeout );
            }
        );
    }
}


void DFL::Power::Module::Wlroots::powerOffDisplays() {
    for ( QString opStr: impl->dpms.keys() ) {
        impl->dpms[ opStr ]->setEnabled( false );
    }
}


void DFL::Power::Module::Wlroots::powerOnDisplays() {
    for ( QString opStr: impl->dpms.keys() ) {
        impl->dpms[ opStr ]->setEnabled( true );
    }
}


qreal DFL::Power::Module::Wlroots::getBrightness() {
    return impl->gammaRefConfig.brightness;
}


void DFL::Power::Module::Wlroots::setBrightness( qreal value ) {
    if ( value > 1.0 ) {
        value = value / 100.0;
    }

    impl->gammaRefConfig.brightness = value;

    for ( QString opStr: impl->brightnessClients ) {
        if ( impl->gammaEffects.contains( opStr ) ) {
            impl->gammaConfigs[ opStr ].brightness = impl->gammaRefConfig.brightness;
            impl->gammaEffects[ opStr ]->setConfiguration( impl->gammaConfigs[ opStr ] );
        }
    }
}


QStringList DFL::Power::Module::Wlroots::brightnessClients() {
    return impl->brightnessClients;
}


void DFL::Power::Module::Wlroots::addBrightnessClient( QString client ) {
    if ( not impl->brightnessClients.contains( client ) ) {
        impl->brightnessClients << client;

        if ( impl->gammaEffects.contains( client ) ) {
            impl->gammaConfigs[ client ].brightness = impl->gammaRefConfig.brightness;
            impl->gammaEffects[ client ]->setConfiguration( impl->gammaConfigs[ client ] );
        }
    }
}


void DFL::Power::Module::Wlroots::removeBrightnessClient( QString client ) {
    if ( impl->brightnessClients.contains( client ) ) {
        impl->brightnessClients.removeAll( client );

        if ( impl->gammaEffects.contains( client ) ) {
            impl->gammaConfigs[ client ].brightness = 1.0;
            impl->gammaEffects[ client ]->setConfiguration( impl->gammaConfigs[ client ] );
        }
    }
}


void DFL::Power::Module::Wlroots::setSunsetMode( bool yes ) {
    /** Current state is the expected state */
    if ( impl->sunsetMode == yes ) {
        return;
    }

    /**
     * Number of steps it takes to go from max <-> min temperature.
     */
    int steps = ceil( (impl->gammaRefConfig.maxTemp - impl->gammaRefConfig.minTemp) / 10.0 );

    /** Switch on the Sunset mode */
    if ( yes ) {
        for ( int i = 0; i < steps; i++ ) {
            for ( QString opStr: impl->gammaEffects.keys() ) {
                impl->gammaConfigs[ opStr ].minTemp -= 10;

                if ( impl->gammaConfigs[ opStr ].minTemp < impl->gammaRefConfig.minTemp ) {
                    impl->gammaConfigs[ opStr ].minTemp = impl->gammaRefConfig.minTemp;
                }

                impl->gammaEffects[ opStr ]->setConfiguration( impl->gammaConfigs[ opStr ] );
            }

            /** Artificial sleep of 50 ms for gradual transition */
            QThread::usleep( 50 );
            qApp->processEvents();
        }
    }

    /** Switch off the Sunset mode */
    else {
        for ( int i = 0; i < steps; i++ ) {
            for ( QString opStr: impl->gammaEffects.keys() ) {
                impl->gammaConfigs[ opStr ].minTemp += 10;

                if ( impl->gammaRefConfig.minTemp > impl->gammaRefConfig.maxTemp ) {
                    impl->gammaRefConfig.minTemp = impl->gammaRefConfig.maxTemp;
                }

                impl->gammaEffects[ opStr ]->setConfiguration( impl->gammaConfigs[ opStr ] );
            }

            /** Artificial sleep of 50 ms for gradual transition */
            QThread::usleep( 50 );
            qApp->processEvents();
        }
    }
}


void DFL::Power::Module::Wlroots::updateGammaConfig( DFL::GammaEffectsConfig cfg ) {
    impl->gammaRefConfig.gamma       = (cfg.gamma == -1 ? impl->gammaRefConfig.gamma : cfg.gamma);
    impl->gammaRefConfig.minTemp     = (cfg.minTemp == -1 ? impl->gammaRefConfig.minTemp : cfg.minTemp);
    impl->gammaRefConfig.maxTemp     = (cfg.maxTemp == -1 ? impl->gammaRefConfig.maxTemp : cfg.maxTemp);
    impl->gammaRefConfig.temperature = (cfg.temperature == -1 ? impl->gammaRefConfig.temperature : cfg.temperature);
    impl->gammaRefConfig.latitude    = (cfg.latitude == -1 ? impl->gammaRefConfig.latitude : cfg.latitude);
    impl->gammaRefConfig.longitude   = (cfg.longitude == -1 ? impl->gammaRefConfig.longitude : cfg.longitude);
    impl->gammaRefConfig.sunrise     = (cfg.sunrise.isValid() ? cfg.sunrise : impl->gammaRefConfig.sunrise);
    impl->gammaRefConfig.sunset      = (cfg.sunset.isValid() ? cfg.sunset : impl->gammaRefConfig.sunset);

    for ( QString opStr: impl->gammaEffects.keys() ) {
        impl->gammaConfigs[ opStr ].gamma       = (cfg.gamma == -1 ? impl->gammaRefConfig.gamma : cfg.gamma);
        impl->gammaConfigs[ opStr ].minTemp     = (cfg.minTemp == -1 ? impl->gammaRefConfig.minTemp : cfg.minTemp);
        impl->gammaConfigs[ opStr ].maxTemp     = (cfg.maxTemp == -1 ? impl->gammaRefConfig.maxTemp : cfg.maxTemp);
        impl->gammaConfigs[ opStr ].temperature = (cfg.temperature == -1 ? impl->gammaRefConfig.temperature : cfg.temperature);
        impl->gammaConfigs[ opStr ].latitude    = (cfg.latitude == -1 ? impl->gammaRefConfig.latitude : cfg.latitude);
        impl->gammaConfigs[ opStr ].longitude   = (cfg.longitude == -1 ? impl->gammaRefConfig.longitude : cfg.longitude);
        impl->gammaConfigs[ opStr ].sunrise     = (cfg.sunrise.isValid() ? cfg.sunrise : impl->gammaRefConfig.sunrise);
        impl->gammaConfigs[ opStr ].sunset      = (cfg.sunset.isValid() ? cfg.sunset : impl->gammaRefConfig.sunset);

        impl->gammaEffects[ opStr ]->setConfiguration( impl->gammaConfigs[ opStr ] );
    }
}


void DFL::Power::Module::Wlroots::handleOutputAdded( WQt::Output *wlOp ) {
    wlOp->waitForReady();

    /** Remove the older WQt::Output object */
    if ( impl->outputs.contains( wlOp->name() ) ) {
        handleOutputRemoved( wlOp );
    }

    /** Add it to the @outputs hash */
    impl->outputs[ wlOp->name() ] = wlOp;

    /** Create the WQt::OutputPower object */
    WQt::OutputPower *pwr = impl->opMgr->getOutputPower( wlOp->get() );

    /** Store the just created WQt::OutputPower object in @dpms hash */
    impl->dpms[ wlOp->name() ] = pwr;

    /** Setup WQt::GammaControl and DFL::GammaEffects objectss and store them */
    // restartGammaEffects( wlOp );
}


void DFL::Power::Module::Wlroots::handleOutputRemoved( WQt::Output *wlOp ) {
    WQt::OutputPower *pwr = impl->dpms.take( wlOp->name() );

    delete pwr;

    /** Stop the gamma effects */
    DFL::GammaEffects *ge = impl->gammaEffects.take( wlOp->name() );

    delete ge;

    /** Destroy the gamma control objects */
    WQt::GammaControl *gc = impl->gammaCtrls.take( wlOp->name() );

    gc->disconnect();
    delete gc;

    /** Remove the output object from @outputs hash, and destroy it */
    QString opStr = impl->outputs.key( wlOp );

    if ( not opStr.isEmpty() ) {
        impl->outputs.remove( opStr );
    }

    delete wlOp;
}


void DFL::Power::Module::Wlroots::restartGammaEffects( WQt::Output *wlOp ) {
    /** Create the gamma control object */
    WQt::GammaControl *gammaCtrl = impl->gammaMgr->getGammaControl( wlOp->get() );

    impl->gammaCtrls[ wlOp->name() ] = gammaCtrl;

    /** Create the gamma effects config for this output */
    DFL::GammaEffectsConfig cfg = impl->gammaRefConfig;

    if ( impl->sunsetMode == false ) {
        cfg.minTemp = cfg.maxTemp;
    }

    /** Not using soft brightness */
    if ( impl->brightnessClients.contains( wlOp->name() ) == false ) {
        cfg.brightness = 1.0;
    }

    impl->gammaConfigs[ wlOp->name() ] = cfg;

    /** Create the gamma effects object for this outupt */
    DFL::GammaEffects *effect = new DFL::GammaEffects( gammaCtrl );

    impl->gammaEffects[ wlOp->name() ] = effect;

    /**
     * If setting the gamma effects fail, restart them.
     * This indeed becomes an infinite loop. How do we handle this
     * situation?
     */
    connect(
        gammaCtrl, &WQt::GammaControl::failed, [ = ]() {
            delete gammaCtrl;
            delete effect;

            // restartGammaEffects( wlOp );
        }
    );

    /** Start the effects */
    effect->setConfiguration( cfg );
}
