/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The UPowerModule watches the org.freedesktop.UPower and
 *  relays the relevent information to DFL::PowerManager class.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>

typedef struct upower_impl_t {
    /** UPower DBus Interface Objects */
    QDBusInterface *upower = nullptr;

    /** Will be useful for showing status messages */
    QDBusInterface *display = nullptr;

    /* State of the lid */
    bool           mLidOpen = false;

    /* State of the power supply */
    bool           mOnBattery = false;

    /* Last battery percentage */
    double         mLastPercentage = 0;

    /* Estimated time remaining */
    int            mETR          = 0;
    bool           mSignalled60  = false;
    bool           mSignalled300 = false;

    /* Estimated time to full */
    int            mETF = 0;
} UPowerImpl;
