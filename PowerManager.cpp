/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Power::Manager class make it convenient to
 **/

#include <unistd.h>
#include <QtDBus>

#include "DFPowerManager.hpp"
#include "PowerManagerImpl.hpp"

#include "UPowerModule.hpp"
#include "WlrootsModule.hpp"

#include <DFLogin1.hpp>

DFL::Power::Manager::Manager( QObject *parent ) : QObject( parent ) {
    impl = new PowerManagerImpl;

    /** Init the UPower Module */
    impl->UPower = new DFL::Power::Module::UPower( this );

    /** Init the Login1 Module */
    impl->Login1 = new DFL::Login1( this );

    /** Init the Wlroots Module */
    impl->Wlroots = new DFL::Power::Module::Wlroots( this );
}


DFL::Power::Manager::~Manager() {
    /** Stop the management */
    stopManagement();

    /** Delete the pointers */
    delete impl->UPower;
    delete impl->Login1;
    delete impl->Wlroots;

    delete impl;
}


DFL::Power::PowerStatus DFL::Power::Manager::powerStatus() {
    PowerStatus status;

    status.powerState    = impl->mCurState;
    status.lidState      = impl->mCurLidState;
    status.batteryCharge = impl->UPower->batteryCharge();
    status.timeToFull    = impl->UPower->getTimeToFull();
    status.timeToEmpty   = impl->UPower->getTimeToEmpty();

    return status;
}


void DFL::Power::Manager::addPowerConfiguration( DFL::Power::PowerState state, DFL::Power::PowerStateConfig cfg ) {
    impl->mPowerConfigs[ state ] = cfg;
}


void DFL::Power::Manager::setBatteryChargeLevel( DFL::Power::ChargeLevel level, double charge ) {
    switch ( level ) {
        case DFL::Power::LowCharge: {
            impl->mLowCharge = charge;
            break;
        }

        case DFL::Power::CriticalCharge: {
            impl->mCriticalCharge = charge;
            break;
        }
    }
}


void DFL::Power::Manager::startManagement() {
    connectSignalsToSlots();

    if ( impl->UPower->onBattery() ) {
        impl->mCurState = DFL::Power::OnBattery;
    }

    else {
        impl->mCurState = DFL::Power::OnAcPower;
    }

    /** If we have a configuration for AC Power enable it */
    if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
        qDebug() << "Loading profile for" << (impl->mCurState == DFL::Power::OnAcPower ? "OnACPower" : "OnBattery");
        loadPowerConfig( impl->mPowerConfigs[ impl->mCurState ] );
    }

    if ( impl->UPower->onBattery() ) {
        emit switchedToBattery();
        emit timeToEmpty( impl->UPower->getTimeToEmpty() );
    }

    else {
        emit timeToFull( impl->UPower->getTimeToFull() );
    }

    /** Send out the battery charge */
    double charge = impl->UPower->batteryCharge();
    emit   batteryChargeChanged( charge );

    connect(
        impl->Wlroots, &DFL::Power::Module::Wlroots::idleTimedOut, [ = ]() {
            if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
                runScript( impl->mPowerConfigs[ impl->mCurState ].IdleScript );
                takeAction( impl->mPowerConfigs[ impl->mCurState ].OnIdle );
            }
        }
    );

    connect(
        impl->Wlroots, &DFL::Power::Module::Wlroots::activityResumed, [ = ]() {
            if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
                runScript( impl->mPowerConfigs[ impl->mCurState ].ResumeScript );
                takeAction( impl->mPowerConfigs[ impl->mCurState ].OnResume );
            }
        }
    );
}


void DFL::Power::Manager::stopManagement() {
    disconnectSignals();
    impl->Wlroots->disconnect();
}


void DFL::Power::Manager::setSunsetMode( bool ) {
    // impl->Wlroots->setSunsetMode( on );
}


void DFL::Power::Manager::updateGammaConfig( DFL::GammaEffectsConfig ) {
    // impl->Wlroots->updateGammaConfig( cfg );
}


void DFL::Power::Manager::setBrightnessBackend( QString display, QString backend ) {
    if ( backend == "gamma" ) {
        impl->Wlroots->addBrightnessClient( display );
    }
}


void DFL::Power::Manager::loadPowerConfig( DFL::Power::PowerStateConfig cfg ) {
    /**
     * Remember the configuration for use elsewhere
     */
    impl->mCfg = cfg;

    /**
     * First thing we do on loading a power config is run the script if it's set.
     * Then we take whatever action specified.
     */
    if ( not cfg.Script.isEmpty() ) {
        runScript( cfg.Script );
    }

    takeAction( cfg.Action );

    /** Next, we set the brightness. */
    qDebug() << "Setting brightness to" << cfg.Brightness << "\b%";
    adjustBrightness( cfg.Brightness );

    /**
     * Stop the previous idleWatch if it was defined
     * If we have a finite idle timeout, implement it.
     */
    if ( cfg.IdleTimeOut ) {
        impl->Wlroots->setupIdleWatch( cfg.IdleTimeOut );
    }
}


void DFL::Power::Manager::takeAction( int action ) {
    /** Perform the action when the configuration is loaded */
    switch ( action ) {
        /** Nothing to do */
        case DFL::Power::NoAction: {
            break;
        }

        case DFL::Power::LoginD: {
            qDebug() << "Leave all management to LoginD";
            break;
        }

        case DFL::Power::DimScreen: {
            qDebug() << "Setting screen brightness to 10%";
            /** Set the brightness to 10% */
            adjustBrightness( 0.10 );
            break;
        }

        case DFL::Power::DPMSOn: {
            qDebug() << "DPMS On";
            /** Attempt to power on all outputs */
            impl->Wlroots->powerOnDisplays();
            break;
        }

        case DFL::Power::DPMSOff: {
            qDebug() << "DPMS Off";
            /** Attempt to power off all outputs */
            impl->Wlroots->powerOffDisplays();
            break;
        }

        case DFL::Power::ShowPowerDialog: {
            /** We don't have to do anything */
            qDebug() << "Showing the power dialog";
            break;
        }

        case DFL::Power::LockScreen: {
            qDebug() << "Lock Screen" << impl->Login1->request( "LockSession" );

            break;
        }

        case DFL::Power::Standby: {
            /** Currently, LoginD does not support this */
            break;
        }

        case DFL::Power::Suspend: {
            qDebug() << "Suspend" << impl->Login1->request( "Suspend" );

            break;
        }

        case DFL::Power::SuspendThenHibernate: {
            qDebug() << "SuspendThenHibernate" << impl->Login1->request( "SuspendThenHibernate" );

            break;
        }

        case DFL::Power::Hibernate: {
            qDebug() << "Hibernate" << impl->Login1->request( "Hibernate" );

            break;
        }

        case DFL::Power::HybridSleep: {
            qDebug() << "HybridSleep" << impl->Login1->request( "HybridSleep" );

            break;
        }

        case DFL::Power::PowerOff: {
            qDebug() << "PowerOff" << impl->Login1->request( "PowerOff" );

            break;
        }

        case DFL::Power::Reboot: {
            qDebug() << "Reboot" << impl->Login1->request( "Reboot" );

            break;
        }
    }
}


void DFL::Power::Manager::runScript( QString script ) {
    qDebug() << "Run Script" << script;

    QFileInfo scriptInfo( script );

    if ( not scriptInfo.isExecutable() ) {
        return;
    }

    QProcess proc;

    proc.setProgram( script );
    proc.setWorkingDirectory( QDir::homePath() );

    /** Start the script */
    proc.start();

    /** Block until it finishes */
    proc.waitForFinished( -1 );
}


void DFL::Power::Manager::handleLidStateChange( bool open ) {
    impl->mCurLidState = (open ? DFL::Power::LidOpen : DFL::Power::LidClosed);

    /** If lid state is not loaded */
    if ( not impl->mCfg.LidConfigs.contains( impl->mCurLidState ) ) {
        return;
    }

    DFL::Power::LidStateConfig lidCfg = impl->mCfg.LidConfigs[ impl->mCurLidState ];

    if ( not lidCfg.Script.isEmpty() ) {
        runScript( lidCfg.Script );
    }

    qDebug() << impl->mCurLidState << DFL::Power::LidOpen;
    takeAction( lidCfg.Action );
}


void DFL::Power::Manager::adjustBrightness( double brightness ) {
    impl->Login1->setBrightness( brightness );

    /** Gamma Control based Brightness Setup */
    // impl->Wlroots->setBrightness( brightness );
}


void DFL::Power::Manager::connectSignalsToSlots() {
    connect( impl->UPower, &DFL::Power::Module::UPower::lidStateChanged, this, &DFL::Power::Manager::handleLidStateChange );

    connect(
        impl->UPower, &DFL::Power::Module::UPower::switchedToACPower, [ = ]() {
            impl->mCurState = DFL::Power::OnAcPower;

            if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
                qDebug() << "Loading profile for OnACPower";
                loadPowerConfig( impl->mPowerConfigs[ impl->mCurState ] );
            }

            // else {
            // impl->Login1->releaseInhibitLocks();
            // }
        }
    );

    connect(
        impl->UPower, &DFL::Power::Module::UPower::switchedToBattery, [ = ]() {
            impl->mCurState = DFL::Power::OnBattery;

            if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
                qDebug() << "Loading profile for OnBattery";
                loadPowerConfig( impl->mPowerConfigs[ impl->mCurState ] );
            }

            // else {
            // impl->Login1->releaseInhibitLocks();
            // }
        }
    );

    connect(
        impl->UPower, &DFL::Power::Module::UPower::batteryChargeChanged, [ = ]( double charge ) {
            DFL::Power::PowerState newState = impl->mCurState;

            /**
             * If we're on AC then no Power State changes.
             * Otherwise, we may go from Battery -> Low Battery -> Critical Battery
             */
            if ( impl->mCurState != DFL::Power::OnAcPower ) {
                if ( charge <= impl->mLowCharge ) {
                    newState = DFL::Power::OnLowBattery;
                }

                else if ( charge <= impl->mCriticalCharge ) {
                    newState = DFL::Power::OnCriticalBattery;
                }
            }

            /** Load the power config only if the state changed. */
            if ( newState != impl->mCurState ) {
                impl->mCurState = newState;

                if ( impl->mPowerConfigs.contains( impl->mCurState ) ) {
                    qDebug() << "Loading profile for" << (newState == DFL::Power::OnLowBattery ? "OnLowBattery" : "OnCriticalBattery");
                    loadPowerConfig( impl->mPowerConfigs[ impl->mCurState ] );
                }

                // else {
                // impl->Login1->releaseInhibitLocks();
                // }
            }
        }
    );

    /** Transmit all the Signals received from DFL::Power::Module::UPower forward via DFL::Power::Manager */
    connect( impl->UPower, &DFL::Power::Module::UPower::lidStateChanged,      this, &DFL::Power::Manager::lidStateChanged );
    connect( impl->UPower, &DFL::Power::Module::UPower::switchedToBattery,    this, &DFL::Power::Manager::switchedToBattery );
    connect( impl->UPower, &DFL::Power::Module::UPower::switchedToACPower,    this, &DFL::Power::Manager::switchedToACPower );
    connect( impl->UPower, &DFL::Power::Module::UPower::batteryChargeChanged, this, &DFL::Power::Manager::batteryChargeChanged );
    connect( impl->UPower, &DFL::Power::Module::UPower::timeToFull,           this, &DFL::Power::Manager::timeToFull );
    connect( impl->UPower, &DFL::Power::Module::UPower::timeToEmpty,          this, &DFL::Power::Manager::timeToEmpty );
    connect( impl->UPower, &DFL::Power::Module::UPower::batteryNearlyEmpty,   this, &DFL::Power::Manager::batteryNearlyEmpty );
    connect( impl->UPower, &DFL::Power::Module::UPower::batteryEmpty,         this, &DFL::Power::Manager::batteryEmpty );
}


void DFL::Power::Manager::disconnectSignals() {
    impl->UPower->disconnect();
}
