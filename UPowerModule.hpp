/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96Abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL Class
 *  The UPowerModule watches the org.freedesktop.UPower and
 *  relays the relevent information to DFL::PowerManager class.
 *  This is purely an implementational detail.
 *  This class can be modified/removed at anytime.
 **/

#pragma once

#include <QtCore>
#include "UPowerModuleImpl.hpp"

namespace DFL {
    namespace Power {
        namespace Module {
            class UPower;
        }
    }
}

class DFL::Power::Module::UPower : public QObject {
    Q_OBJECT;

    public:
        UPower( QObject *parent = nullptr );

        qlonglong getTimeToFull();
        qlonglong getTimeToEmpty();
        bool onBattery();
        double batteryCharge();

    private:
        UPowerImpl *impl;

        /**
         * This function will handle all the power changes,
         * and emit suitable signals.
         */
        Q_SLOT void handlePowerChanges( QString, QVariantMap, QStringList );

    Q_SIGNALS:

        /* Lid Open/Close Signals */
        void lidStateChanged( bool open );

        /* Battery Charge/Discharge Signals */
        void switchedToBattery();
        void switchedToACPower();

        /* Battery charge changed */
        void batteryChargeChanged( double );

        void timeToFull( qlonglong );
        void timeToEmpty( qlonglong );

        /**
         * Battery nearly and almost empty signals
         * Battery nearly empty - 5m or 5% whichever is less
         * Battery nearly empty - 1m or 1% whichever is less
         */
        void batteryNearlyEmpty();
        void batteryEmpty();
};
